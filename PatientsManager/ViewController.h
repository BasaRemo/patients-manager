//
//  ViewController.h
//  HealthApp
//
//  Created by Ntambwa Basambombo on 2015-02-07.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FYX/FYX.h>
#import <FYX/FYXVisitManager.h>
#import <FYX/FYXTransmitter.h>
#import <FYX/FYXSightingManager.h>
#import <FYX/FYXTransmitter.h>
#import <FYX/FYXVisit.h>

@interface ViewController : UIViewController <FYXServiceDelegate,FYXVisitDelegate>


@end

