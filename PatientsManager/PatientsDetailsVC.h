//
//  PatientsDetailsVC.h
//  PatientsManager
//
//  Created by Ntambwa Basambombo on 2015-02-07.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <FlatUIKit/UIColor+FlatUI.h>
#import "UIColor+FlatUI.h"
#import "FUIButton.h"
#import "UIFont+FlatUI.h"

@interface PatientsDetailsVC : UIViewController <UITableViewDataSource,UITableViewDelegate>
- (IBAction)doneBtnTapped:(id)sender;

- (IBAction)dismissBtnTapped:(id)sender;
@property (strong, nonatomic) id detailItem;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) UIImage *picture;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
