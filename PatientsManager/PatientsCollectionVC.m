//
//  PatientsCollectionVC.m
//  PatientsManager
//
//  Created by Ntambwa Basambombo on 2015-02-07.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import "PatientsCollectionVC.h"

@interface PatientsCollectionVC ()
@end

@implementation PatientsCollectionVC
@synthesize menuView,tabView;
static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    NSDate *date = [[NSDate alloc] init];
    
    
    self.currentDate.text = [dateFormatter stringFromDate:date];
    [self retrieveFromParse];
    [self setCustomTabBar];
    
    //Start Gimbal Service
    [FYX startService:self];
    
    // Do any additional setup after loading the view.
}

/****** GIMBAL *******/

- (void)serviceStarted
{
    // this will be invoked if the service has successfully started
    // bluetooth scanning will be started at this point.
    NSLog(@"FYX Service Successfully Started");
    
    self.visitManager = [FYXVisitManager new];
    self.visitManager.delegate = self;
    NSMutableDictionary *options = [NSMutableDictionary new];
    [options setObject:[NSNumber numberWithInt:1] forKey:FYXVisitOptionDepartureIntervalInSecondsKey];
    [options setObject:[NSNumber numberWithInt:FYXSightingOptionSignalStrengthWindowNone] forKey:FYXSightingOptionSignalStrengthWindowKey];
    [options setObject:[NSNumber numberWithInt:-55] forKey:FYXVisitOptionArrivalRSSIKey];
    [options setObject:[NSNumber numberWithInt:-65] forKey:FYXVisitOptionDepartureRSSIKey];
    [self.visitManager startWithOptions:options];
}
- (void)startServiceFailed:(NSError *)error
{
    // this will be called if the service has failed to start
    NSLog(@"%@", error);
}

- (void)didArrive:(FYXVisit *)visit
{
    // this will be invoked when an authorized transmitter is sighted for the first time
    NSLog(@"I arrived at a Gimbal Beacon!!! %@", visit.transmitter.name);
}
- (void)receivedSighting:(FYXVisit *)visit updateTime:(NSDate *)updateTime RSSI:(NSNumber *)RSSI;
{
    // this will be invoked when an authorized transmitter is sighted during an on-going visit
    //FYXTransmitter *transmitter = [FYXTransmitter new];
    if (!_vc) {
        [self performSegueWithIdentifier:@"showPatientDetails" sender:self];
    }
    //while (_counter<5) {
        NSLog(@"I received a sighting!!! %@ : %@", visit.transmitter.name,RSSI);
        _counter++;
    //}
    if ([visit.transmitter.name  isEqual:@"DanBeacon"]) {
        tabView.backgroundColor = [UIColor greenSeaColor];
        tabView.enabledTabBackgrondColor = [UIColor greenSeaColor];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"changeBackgroundGREEN" object:nil];
    }
    /*if([visit.transmitter.name  isEqual:@"Basa Beacon"]){
        tabView.backgroundColor = [UIColor belizeHoleColor];
        tabView.enabledTabBackgrondColor = [UIColor belizeHoleColor];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"changeBackgroundBLUE" object:nil];
        //menuVC.tableView.backgroundColor = [UIColor belizeHoleColor];
    }*/

}
- (void)didDepart:(FYXVisit *)visit;
{
    // this will be invoked when an authorized transmitter has not been sighted for some time
    NSLog(@"I left the proximity of a Gimbal Beacon!!!! %@", visit.transmitter.name);
    NSLog(@"I was around the beacon for %f seconds", visit.dwellTime);
    
    [_vc dismissViewControllerAnimated:YES completion:nil];
    _vc = nil;
    
}

/******* Parse server Data **********/

- (void) retrieveFromParse {
    
    PFQuery *retrieveStudents = [PFQuery queryWithClassName:@"Student"];
    
    [retrieveStudents findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            patientsArray = [[NSMutableArray alloc] initWithArray:objects];
            //NSLog(@"Objects Retrieved! %@",patientsArray);
            
        }
        [self.collectionView reloadData];
    }];
    [_vc dismissViewControllerAnimated:YES completion:nil];
}
/******** TAB BAR ********/

-(void) setCustomTabBar{
    
    // Tab Bar 1
    RKTabItem *tabItem1 = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"icon-checkbox-selected-green-25x25@2x.png"] imageDisabled:[UIImage imageNamed:@"icon-checkbox-unselected-25x25.png"]];
    tabItem1.tabState = TabStateEnabled;
    RKTabItem *tabItem2 = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"icon-checkbox-unselected-25x25.png"] imageDisabled:[UIImage imageNamed:@"icon-checkbox-unselected-25x25.png"]];
    RKTabItem *tabItem3 = [RKTabItem createUsualItemWithImageEnabled:[UIImage imageNamed:@"icon-checkbox-selected-green-25x25@2x.png"] imageDisabled:[UIImage imageNamed:@"icon-checkbox-unselected-25x25.png"]];
    
    tabView = [[RKTabView alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width-249,60)];
    
    tabView.delegate = self;
    tabView.backgroundColor = [UIColor alizarinColor];
    tabView.enabledTabBackgrondColor = [UIColor alizarinColor];
    tabView.tabItems = @[tabItem1, tabItem2,tabItem3];
    tabItem1.tabState = TabStateEnabled;
    
    //tabView.horizontalInsets = HorizontalEdgeInsetsMake(10, 10);
    tabView.darkensBackgroundForEnabledTabs = YES;
    tabView.drawSeparators = YES;
    [self.view addSubview:tabView];

}

#pragma mark - RKTabViewDelegate

- (void)tabView:(RKTabView *)tabView tabBecameEnabledAtIndex:(int)index tab:(RKTabItem *)tabItem {
    
    NSLog(@"Tab № %d became enabled on tab view", index);
    switch (index) {
        case 0:
            tabView.backgroundColor = [UIColor alizarinColor];
            tabView.enabledTabBackgrondColor = [UIColor alizarinColor];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"changeBackgroundRED" object:nil];
            //menuVC.tableView.backgroundColor = [UIColor alizarinColor];
            break;
        case 1:
            tabView.backgroundColor = [UIColor greenSeaColor];
            tabView.enabledTabBackgrondColor = [UIColor greenSeaColor];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"changeBackgroundGREEN" object:nil];
            //menuVC.tableView.backgroundColor = [UIColor greenSeaColor];
            break;
        case 2:
            tabView.backgroundColor = [UIColor belizeHoleColor];
            tabView.enabledTabBackgrondColor = [UIColor belizeHoleColor];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"changeBackgroundBLUE" object:nil];
            //menuVC.tableView.backgroundColor = [UIColor belizeHoleColor];
            break;
            
        default:
            tabView.backgroundColor = [UIColor alizarinColor];
            tabView.enabledTabBackgrondColor = [UIColor alizarinColor];
            break;
    }

}

- (void)tabView:(RKTabView *)tabView tabBecameDisabledAtIndex:(int)index tab:(RKTabItem *)tabItem {
    NSLog(@"Tab № %d became disabled on tab view", index);
}



/*********** Patients Collection View ********/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [patientsArray count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    UIImageView *patientPhoto = (UIImageView *)[cell viewWithTag:100];
    UILabel *patientName = (UILabel*)[cell viewWithTag:101];
    UIImageView *patientState = (UIImageView *)[cell viewWithTag:102];
    //NSLog(@"%@",patientName.text);
    PFObject *tempObject = [patientsArray objectAtIndex:indexPath.row];
    patientName.text = [NSString stringWithFormat: @"%@", [tempObject objectForKey:@"name"]];
    //[NSString stringWithFormat: @"%d", attendees];
    if ([tempObject objectForKey:@"sex"]) {
        patientPhoto.image = [UIImage imageNamed:@"user_man_blue.png"];
        patientState.image = [UIImage imageNamed:@"icon-checkbox-unselected-25x25.png"];
    }else{
        patientPhoto.image = [UIImage imageNamed:@"user_woman_blue.png"];
        patientState.image =  [UIImage imageNamed:@"icon-checkbox-selected-green-25x25.png"];
    }
    //[cell.layer setBorderWidth:2.0f];
    //[cell.layer setBorderColor:[UIColor blackColor].CGColor];
    //[cell.layer setCornerRadius:50.0f];
    
    return cell;
}

// Collection View Touch FeedBack

 - (void)collectionView:(UICollectionView *)colView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
 UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
 //set color with animation
 [UIView animateWithDuration:0.1 delay:0 options:(UIViewAnimationOptionAllowUserInteraction)
 animations:^{
 [cell setBackgroundColor:[UIColor colorWithRed:232/255.0f green:232/255.0f blue:232/255.0f alpha:1]];
 }
 completion:nil];
 }
 
 - (void)collectionView:(UICollectionView *)colView  didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
 UICollectionViewCell* cell = [colView cellForItemAtIndexPath:indexPath];
 //set color with animation
 [UIView animateWithDuration:0.1
 delay:0
 options:(UIViewAnimationOptionAllowUserInteraction)
 animations:^{
 [cell setBackgroundColor:[UIColor clearColor]];
 }
 completion:nil ];
 }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    
    if (!_vc) {
        // Pass the selected object to the new view controller.
        if ([[segue identifier] isEqualToString:@"showPatientDetails"]) {
            _vc = [segue destinationViewController];
            
            NSArray *selectedArray = [self.collectionView indexPathsForSelectedItems];
            //NSIndexPath *indexPath = selectedArray[0];
            PFObject *tempObject = [patientsArray objectAtIndex:2];
            [_vc setDetailItem:tempObject];
            
            //PFObject *tempObject = [patientsArray objectAtIndex:indexPath.row];
        }
    }

    
}


@end
