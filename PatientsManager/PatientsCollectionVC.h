//
//  PatientsCollectionVC.h
//  PatientsManager
//
//  Created by Ntambwa Basambombo on 2015-02-07.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "Canvas.h"
#import <FlatUIKit/UIColor+FlatUI.h>
#import "UIColor+FlatUI.h"
#import "FUIButton.h"
#import "UIFont+FlatUI.h"
#import "RKTabView.h"
#import "MenuVC.h"
#import "PatientsDetailsVC.h"

#import <FYX/FYX.h>
#import <FYX/FYXVisitManager.h>
#import <FYX/FYXTransmitter.h>
#import <FYX/FYXSightingManager.h>
#import <FYX/FYXTransmitter.h>
#import <FYX/FYXVisit.h>

 

@interface PatientsCollectionVC : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource,RKTabViewDelegate,FYXServiceDelegate,FYXVisitDelegate>
{
    NSMutableArray *patientsArray;
}
@property (weak, nonatomic) IBOutlet UILabel *currentDate;
@property (weak, nonatomic) IBOutlet UITableView *menuView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) PatientsDetailsVC *vc;
@property (strong, nonatomic) RKTabView *tabView;

@property (nonatomic) FYXVisitManager *visitManager;
@property (nonatomic) int counter;
@end
