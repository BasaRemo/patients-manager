//
//  MenuVC.h
//  Brawlers Manager
//
//  Created by Ntambwa Basambombo on 2015-02-06.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <FlatUIKit/UIColor+FlatUI.h>
#import "UIColor+FlatUI.h"
#import "FUIButton.h"
#import "UIFont+FlatUI.h"

@interface MenuVC : UITableViewController
@property (strong, nonatomic) id detailItem;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) UIImage *picture;
@end
