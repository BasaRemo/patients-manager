//
//  AppDelegate.m
//  PatientsManager
//
//  Created by Ntambwa Basambombo on 2015-02-07.
//  Copyright (c) 2015 Ntambwa Basambombo. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>
#import <FYX/FYX.h>
#import <FYX/FYXLogging.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    //Initialise Gimbal
    [FYX setAppId:@"ad9d628351adb11fe1094bdab8331fe5979bb2143e81673a65f16b51d417b7ac"
        appSecret:@"a27c06f0c44b6eaf7203983819fd00c0c6bc11a89f8fe2990d25507c8603f284"
      callbackUrl:@"a27c06f0c44b6eaf7203983819fd00c0c6bc11a89f8fe2990d25507c8603f284"];
    
    // Initialize Parse.
    [Parse setApplicationId:@"ygqspF2dFqO46y88cl5gRDEkTwCNBhM85CFTPKkm"
                  clientKey:@"25k2mfBf8ejT5KgPv3qMdhr8mNRlfVOr1F7QjfXe"];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
