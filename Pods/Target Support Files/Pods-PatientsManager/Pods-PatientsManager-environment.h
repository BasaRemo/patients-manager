
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Canvas
#define COCOAPODS_POD_AVAILABLE_Canvas
#define COCOAPODS_VERSION_MAJOR_Canvas 0
#define COCOAPODS_VERSION_MINOR_Canvas 1
#define COCOAPODS_VERSION_PATCH_Canvas 2

// FlatUIKit
#define COCOAPODS_POD_AVAILABLE_FlatUIKit
#define COCOAPODS_VERSION_MAJOR_FlatUIKit 1
#define COCOAPODS_VERSION_MINOR_FlatUIKit 6
#define COCOAPODS_VERSION_PATCH_FlatUIKit 0

// RKTabView
#define COCOAPODS_POD_AVAILABLE_RKTabView
#define COCOAPODS_VERSION_MAJOR_RKTabView 1
#define COCOAPODS_VERSION_MINOR_RKTabView 0
#define COCOAPODS_VERSION_PATCH_RKTabView 1

// RSDayFlow
#define COCOAPODS_POD_AVAILABLE_RSDayFlow
#define COCOAPODS_VERSION_MAJOR_RSDayFlow 0
#define COCOAPODS_VERSION_MINOR_RSDayFlow 7
#define COCOAPODS_VERSION_PATCH_RSDayFlow 3

